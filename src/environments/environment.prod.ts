export const environment = {
  production: true,
  pokemonAPI: 'https://pokeapi.co/api/v2',
  trainerApiBaseUrl: 'http://localhost:3000'
};
