
export const environment = {
  production: false,
  pokemonApiBaseUrl: 'https://pokeapi.co/api/v2/',
  trainerApiBaseUrl: 'http://localhost:3001'
};

