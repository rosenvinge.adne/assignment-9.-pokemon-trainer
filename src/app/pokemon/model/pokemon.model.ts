export interface PokemonAbilityAbility{
  name: string;
  url: string;
}

export interface PokemonAbility{
  ability: PokemonAbilityAbility;
  is_hidden: boolean;
  slot: number;
}

export interface PokemonStatStat{
  name: string;
  base_stat: number;
  url: string;
}

export interface PokemonStat{
  base_stat: number;
  effort: number;
  stat: PokemonStatStat;
}

export interface PokemonMove{
  move: PokemonMoveMove;
}

export interface PokemonMoveMove{
  name: string;
  url: string;
}



export interface PokemonTypeType{
  name: string;
  url: string;
}

export interface PokemonType{
  slot: string;
  type: PokemonTypeType;
}

export interface Pokemon{
  name: string;
  url: string;
  id?: string;
  image?: string;
  abilities?: PokemonAbility[];
  moves?: PokemonMove[];
  types?: PokemonType[];
  stats?: PokemonStat[];
  weight?: number;
  height?: number;
  base_experience: number;

}
