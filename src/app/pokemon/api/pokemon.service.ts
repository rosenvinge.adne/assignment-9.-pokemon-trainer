import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {map, shareReplay} from 'rxjs/operators';
import {Pokemon} from '../model/pokemon.model';

const {pokemonApiBaseUrl} = environment;

@Injectable({
  providedIn: 'root'
})
export class PokemonService {

  pokemonList: Pokemon[] = [];
  error: string = '';
  fetchOffset: number = 0;

  constructor(private readonly http: HttpClient) {
  }

  public fetchPokemon$(offset: number): void {
    this.fetchOffset = offset;
    this.http.get(`${pokemonApiBaseUrl}pokemon?limit=20&offset=${this.fetchOffset}`)
      .pipe(
        map((response: any) =>
          response.results.map(pokemon => ({
            ...pokemon,
            ...this.getIdAndImage(pokemon.url)
          }))
        ))
      .subscribe(
        (pokemonList) => {
          console.log(pokemonList);
          this.pokemonList = pokemonList;
        },
        (errorResponse: HttpErrorResponse) => {
          this.error = errorResponse.message;
          console.log(this.error);
        });
  }


  public getPokemonById$(id: string): any {
    return this.http.get(`${pokemonApiBaseUrl}pokemon/${id}`);
  }

  private getIdAndImage(url: string): any {
    const id = url.split('/').filter(Boolean).pop();
    return {id, image: `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${id}.png`};
  }
}

