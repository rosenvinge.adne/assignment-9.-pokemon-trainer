import {RouterModule, Routes} from "@angular/router";
import {PokemonDetail} from "./components/detail/detail.component";
import {NgModule} from "@angular/core";

const routes: Routes = [
  {
    path: ':id',
    component: PokemonDetail
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
  ],
  exports: [
    RouterModule
  ]
})

export class DetailRoutingModule{
}
