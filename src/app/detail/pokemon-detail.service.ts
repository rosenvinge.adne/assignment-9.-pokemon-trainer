import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {Pokemon} from '../pokemon/model/pokemon.model';
import {map} from 'rxjs/operators';

const { pokemonApiBaseUrl} = environment;
@Injectable({
  providedIn: 'root'
})
export class PokemonDetailService {
  pokemon: Pokemon;
  error: string = '';

  constructor(private readonly http: HttpClient) {

  }

  public getPokemonById$(id: number): void {
    this.http.get(`${pokemonApiBaseUrl}pokemon/${id}`)
      .pipe(
        map((pokemon: Pokemon) => ({
          ...pokemon,
          image: `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${ pokemon.id }.png`
        })))
      .subscribe(
        (pokemon: Pokemon) => {
          console.log('From PokemonDetailService', pokemon);
          this.pokemon = pokemon;
        },
        (errorResponse: HttpErrorResponse) => {
          console.log(errorResponse.message);
        });
  }
}
