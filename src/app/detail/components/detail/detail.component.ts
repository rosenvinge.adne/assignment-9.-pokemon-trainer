import {Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute, ParamMap} from '@angular/router';
import {PokemonService} from '../../../pokemon/api/pokemon.service';
import {Pokemon} from '../../../pokemon/model/pokemon.model';
import {PokemonDetailService} from '../../pokemon-detail.service';


@Component({
  selector: 'app-pokemon-detail',
  templateUrl: './detail.component.html',
})

export class PokemonDetail implements OnInit{
  private readonly id: string = 'placeholder ID';
  constructor(private route: ActivatedRoute, private readonly pokeService: PokemonDetailService) {
    this.id = this.route.snapshot.paramMap.get('id');
  }


  ngOnInit(): void {
    this.pokeService.getPokemonById$(Number(this.id));
  }

  get pokemon(): Pokemon{
    return this.pokeService.pokemon;
  }
}


