import {NgModule} from '@angular/core';
import {PokemonDetail} from './components/detail/detail.component';
import {DetailRoutingModule} from "./detail-routing.module";
import {MatCardModule} from "@angular/material/card";
import {CommonModule} from "@angular/common";


@NgModule({
  declarations: [
    PokemonDetail
  ],
  imports: [
    DetailRoutingModule,
    MatCardModule,
    CommonModule
  ]
})

export class DetailModule{}
