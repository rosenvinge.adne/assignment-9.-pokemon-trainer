import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AppRoutes} from './shared/enums/app-routes.enum';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: `/${AppRoutes.Login}`
  },
  {
    path: AppRoutes.Login,
    loadChildren: () => import('./login/login.module').then(m => m.LoginModule)
  },
  {
    path: AppRoutes.PokemonCatalogue,
    loadChildren: () => import('./catalogue/catalogue.module').then(m => m.CatalogueModule)
  },
  {
    path: AppRoutes.TrainerPage,
    loadChildren: () => import('./trainer/trainer.module').then(m => m.TrainerModule)
  },
  {
    path: AppRoutes.PokemonDetail,
    loadChildren: () => import('./detail/detail.module').then(m => m.DetailModule)
  }

];

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {}
