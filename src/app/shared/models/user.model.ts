export interface User {
  id: string;
  name: string;
  pokemon: PokemonId[];
}

export interface PokemonId {
  id: string;
}
