
export enum AppRoutes {
  Login = 'login',
  PokemonCatalogue = 'pokedex',
  PokemonDetail = 'pokemon-detail', // /{pokemon-id}
  TrainerPage = 'my-page'

}
