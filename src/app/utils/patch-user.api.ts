import {Injectable} from '@angular/core';
import {User} from '../shared/models/user.model';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PatchUserApi {
  constructor(private readonly http: HttpClient) {
  }

  public updateUser$(user: User): void {
    this.http.patch(`${environment.trainerApiBaseUrl}/trainers/${user.id}`, {
      pokemon: user.pokemon
    })
      .subscribe(
        (val) => {
          console.log('PATCH call successful value returned in body',
            val);
        },
        response => {
          console.log('PATCH call in error', response);
        },
        () => {
          console.log('The PATCH observable is now completed.');
        });
  }
}
