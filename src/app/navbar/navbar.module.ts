import {NgModule} from '@angular/core';
import {NavbarComponent} from './components/navbar.component';
import {CommonModule} from '@angular/common';
import {MatButtonModule} from '@angular/material/button';


@NgModule({
  declarations: [
    NavbarComponent
  ],
  exports: [
    NavbarComponent
  ],
  imports: [
    CommonModule,
    MatButtonModule
  ]
})

export class NavbarModule{}
