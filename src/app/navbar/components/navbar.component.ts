import {Component} from '@angular/core';
import {Router} from '@angular/router';
import {AppRoutes} from '../../shared/enums/app-routes.enum';
import {getStorage} from '../../utils/storage.utils';
import {User} from '../../shared/models/user.model';
import {LocalStorageKeys} from '../../shared/enums/local-storage-keys.enum';


@Component({
  selector: 'app-navigation-bar',
  template: `
    <div>
      <button type="button" mat-raised-button (click)="goToCatalogue()">Pokedex</button>
      <button type="button" mat-raised-button (click)="goToMyPage()">Trainer Page</button>
    </div>
  `
})

export class NavbarComponent {

  constructor(private readonly router: Router) {
  }

  // Navbar does not work if the user is not logged inn

  goToCatalogue(): Promise<boolean> {
    if (!getStorage<User>(LocalStorageKeys.USER)) {
      return null;
    }
    return this.router.navigate([AppRoutes.PokemonCatalogue]);
  }

  goToMyPage(): Promise<boolean> {
    if (!getStorage<User>(LocalStorageKeys.USER)) {
      return null;
    }
    return this.router.navigate([AppRoutes.TrainerPage]);
  }


}
