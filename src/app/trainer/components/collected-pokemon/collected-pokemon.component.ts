import {Component, OnInit} from '@angular/core';
import {Pokemon} from '../../../pokemon/model/pokemon.model';
import {getStorage, setStorage} from '../../../utils/storage.utils';
import {LocalStorageKeys} from '../../../shared/enums/local-storage-keys.enum';
import {PokemonId, User} from '../../../shared/models/user.model';
import {PokemonService} from '../../../pokemon/api/pokemon.service';
import {HttpErrorResponse} from '@angular/common/http';
import {AppRoutes} from '../../../shared/enums/app-routes.enum';
import {Router} from '@angular/router';
import {PatchUserApi} from '../../../utils/patch-user.api';


@Component({
  selector: 'app-trainer-pokemon-collection',
  templateUrl: 'collected-pokemon.component.html'
})

export class CollectedPokemonComponent implements OnInit {
  user: User;
  collectedPokemon: any = [];

  constructor(private readonly pokeService: PokemonService,
              private readonly router: Router,
              private readonly userApi: PatchUserApi) {
    this.user = getStorage<User>(LocalStorageKeys.USER);
  }

  ngOnInit(): void {
    if (this.user.pokemon) {
      this.user.pokemon.forEach(item => {

        this.pokeService.getPokemonById$(item.id)
          .subscribe((pokemon: Pokemon) => {
              this.collectedPokemon.push({
                ...pokemon,
                image: `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${ pokemon.id }.png`
              });
            }, (error: HttpErrorResponse) => console.log(error)
          );

      });
    }
  }
  goToPokemonDetail(id: string): Promise<boolean> {
    return this.router.navigateByUrl(`${AppRoutes.PokemonDetail}/${id}`);
  }

}


