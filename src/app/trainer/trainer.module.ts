import {NgModule} from '@angular/core';
import {TrainerPage} from './pages/trainer-page/trainer.page';
import {CommonModule} from '@angular/common';
import {CollectedPokemonComponent} from './components/collected-pokemon/collected-pokemon.component';
import {TrainerRoutingModule} from './trainer-routing.module';
import {MatCardModule} from '@angular/material/card';

@NgModule({
  declarations: [
    TrainerPage,
    CollectedPokemonComponent
  ],
  imports: [
    CommonModule,
    TrainerRoutingModule,
    MatCardModule
  ]
})

export class TrainerModule {
}
