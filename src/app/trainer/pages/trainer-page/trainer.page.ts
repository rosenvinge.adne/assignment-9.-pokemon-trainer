import {Component, OnInit} from '@angular/core';

import {AppRoutes} from '../../../shared/enums/app-routes.enum';
import {Router} from '@angular/router';

@Component({
  selector: 'app-trainer-page',
  templateUrl: 'trainer.page.html'

})
export class TrainerPage implements OnInit{

  constructor(private readonly router: Router) {
  }

  ngOnInit() {
  }

  viewPokemon(): Promise<boolean> { // Click on pokemon
    console.log('LoginPage.viewPokemon()');
    return this.router.navigate([AppRoutes.PokemonDetail, '1']);
    // TODO: This is hardcoded
  }

}
