import {Injectable} from '@angular/core';
import {LoginApi} from './api/login.api';
import {HttpErrorResponse} from '@angular/common/http';
import {User} from '../shared/models/user.model';
import {finalize, map} from 'rxjs/operators';
import {LoginState} from './state/login.state';
import {Observable} from 'rxjs';
import {setStorage} from '../utils/storage.utils';
import {LocalStorageKeys} from '../shared/enums/local-storage-keys.enum';

@Injectable({
  providedIn: 'root'
})
export class LoginFacade {
  constructor(private readonly loginAPI: LoginApi, private readonly loginState: LoginState) {
  }

  public error$(): Observable<string> {
    return this.loginState.getError$();

  }

  public user$(): Observable<User> {
    return this.loginState.getUser$();
  }

  public loading$(): Observable<boolean> {
    return this.loginState.getLoading$();
  }

  private setUser(user: User): void {
    setStorage<User>(LocalStorageKeys.USER, user);
  }


  public login(username: string): void {

    this.loginState.setLoading(true);

    this.loginAPI.login$(username)
      .pipe(
        map((response: User[]) => {
          if (response.length === 0) {
            throw Error(`User ${username} was not found.`); // TODO: 'addNewUser()' ??
          }
          return response.pop();
        }),
        finalize(() => {
          this.loginState.setLoading(false);
        })
      )
      .subscribe((response: User) => {
        console.log('LoginFacade.login()', response);

        this.setUser(response);

        this.loginState.setUser(response);
      }, (error: HttpErrorResponse) => {
        console.error(error);
        this.loginState.setError(error.message);
      });
  }
}
