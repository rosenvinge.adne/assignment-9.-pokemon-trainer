import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {User} from '../../shared/models/user.model';

const {trainerApiBaseUrl} = environment;


@Injectable({
  providedIn: 'root'
})
export class LoginApi{

  constructor(private readonly http: HttpClient) {
  }

  public login$(username: string): Observable<User[]>{
    return this.http.get<User[]>(`${trainerApiBaseUrl}/trainers?name=${username}`);
  }
}
