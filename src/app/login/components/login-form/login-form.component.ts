import {Component, EventEmitter, OnDestroy, Output} from '@angular/core';
import {LoginFacade} from '../../login.facade';
import {Observable, Subscription} from 'rxjs';
import {User} from '../../../shared/models/user.model';

@Component({
  selector: 'app-login-form',
  templateUrl: 'login-form.component.html'

})
export class LoginFormComponent implements OnDestroy{

  @Output() loggedInn: EventEmitter<User> = new EventEmitter<User>();

  public username: string = '';

  private user$: Subscription;

  constructor(private readonly loginFacade: LoginFacade) {
    this.user$ = this.loginFacade.user$().subscribe( (user: User) => {
      if (user === null){ return; }
      this.loggedInn.emit(user);
    });
  }

  get error$(): Observable<string> {
    return this.loginFacade.error$();
  }

  get loading$(): Observable<boolean>{
    return this.loginFacade.loading$();
  }

  public onLoginClick(): void {

    console.log('LoginFormComponent.onLoginClick.username', this.username);

    this.loginFacade.login(this.username);
  }


  ngOnDestroy(): void {
    this.user$.unsubscribe();
  }

}

