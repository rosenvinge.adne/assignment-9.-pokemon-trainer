import {NgModule} from '@angular/core';
import {LoginRoutingModule} from './login-routing.module';
import {LoginPage} from './pages/login-page/login.page';
import {LoginFormComponent} from './components/login-form/login-form.component';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {MatButtonModule} from '@angular/material/button';

@NgModule({
  declarations: [
    LoginPage,
    LoginFormComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    LoginRoutingModule,
    MatButtonModule
  ]
})

export class LoginModule {
}
