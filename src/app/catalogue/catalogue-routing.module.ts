import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {CataloguePage} from './pages/catalogue-page/catalogue.page';
import {PokemonDetail} from '../detail/components/detail/detail.component';
import {MatCardModule} from '@angular/material/card';
import {CommonModule} from '@angular/common';

const routes: Routes = [
  {
    path: '',
    component: CataloguePage
  },
  {
    path: ':id',
    component: PokemonDetail
  }
];
@NgModule({
  imports: [
    RouterModule.forChild(routes),
    MatCardModule,
    CommonModule
  ],
  exports: [
    RouterModule
  ]
})

export class CatalogueRoutingModule {
}
