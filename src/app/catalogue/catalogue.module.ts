import {NgModule} from '@angular/core';
import {CataloguePage} from './pages/catalogue-page/catalogue.page';
import {CommonModule} from '@angular/common';
import {CatalogueRoutingModule} from './catalogue-routing.module';
import {MatCardModule} from '@angular/material/card';
import {MatButtonModule} from '@angular/material/button';


@NgModule({
  declarations: [
    CataloguePage
  ],
    imports: [
        CommonModule,
        CatalogueRoutingModule,
        MatCardModule,
        MatButtonModule
    ]
})

export class CatalogueModule{}
