import {Component, OnInit} from '@angular/core';
import {PokemonService} from '../../../pokemon/api/pokemon.service';
import {Pokemon} from '../../../pokemon/model/pokemon.model';
import {Router} from '@angular/router';
import {AppRoutes} from '../../../shared/enums/app-routes.enum';
import {getStorage, setStorage} from '../../../utils/storage.utils';
import {PokemonId, User} from 'src/app/shared/models/user.model';
import {LocalStorageKeys} from '../../../shared/enums/local-storage-keys.enum';
import {PatchUserApi} from '../../../utils/patch-user.api';


@Component({
  selector: 'app-catalogue-page',
  templateUrl: 'catalogue.page.html',
  styleUrls: ['./catalogue.page.scss']
})

export class CataloguePage implements OnInit {
  offset: number = 0;

  constructor(private readonly pokeService: PokemonService,
              private readonly router: Router,
              private readonly userApi: PatchUserApi) {
  }

  get pokemon(): Pokemon[] {
    return this.pokeService.pokemonList;
  }

  ngOnInit(): void {
    this.pokeService.fetchPokemon$(this.offset);
  }
  goToPokemonDetail(id: string): Promise<boolean> {
    return this.router.navigateByUrl(`${AppRoutes.PokemonDetail}/${id}`);
  }

  collectPokemon(pokemonId: string): void {
    const user: User = getStorage<User>(LocalStorageKeys.USER);
    user.pokemon.push({id: pokemonId});
    setStorage<User>(LocalStorageKeys.USER, user);

    console.log('Pokemon collected', pokemonId);

    this.userApi.updateUser$(user);
  }

  nextPage(): void{
    if (this.offset < 1090){
      this.offset += 20;
      this.pokeService.fetchPokemon$(this.offset);
    }
  }
  lastPage(): void{
    if (this.offset > 0){
      this.offset -= 20;
      this.pokeService.fetchPokemon$(this.offset);
    }
  }
}
