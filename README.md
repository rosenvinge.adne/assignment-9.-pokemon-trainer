# Pokemon trainer app - Angular
#### By Ådne and Pranav

## About

This is a Angular progjects that using a pokemon api and a local JSON database to let the user log in, view pokemon and "capture" them.

Json server repo: https://gitlab.com/rosenvinge.adne/pokemon-jsonserver
This server needs to run on localhost:3001 for the application to work.

We didn't have time to implement alll the functonality we wanted, but the minimal requirements should be in order.

## Pattern

We tryed to follow the pattern given, but we ended up not being perfectly consistent with the naming convention. For example the api file is some times called service and some times api. With more time this is one of the things we would change.

## Main Parts

#### Login

src/login

Contains the login page, then redirects you to the Pokedex

#### Navbar

src/Navbar

After loging inn you can use the navigation bar to navigate to 'My page' and the 'pokedex'

#### Pokedex

src/catalogue

contains a display og pokemon 20 at the time. By clicking Next and Previous you can navigate through different pokemon.

By clicking on a pokemon you go to the pokemon detail page. You can also collect pokemon by clicking on the 'COLLECT' button. This adds the pokemon to your profile in the JSON database.

#### Pokemon detail

src/detail

This page shows more detailed information about each pokemon. 

#### My page

src/trainer

displays the pokemon you have collected. By clicking on any of them you go to the pokemon detailed page.


